# TelegramToWhatsApp

## Prerequisites
```
- Windows 10
- WhatsApp Desktop ver. 2.2202.12
- Chrome ver. < 97.xx
- Microsoft Excel
```

## Depedencies
```
- "UiPath.Excel.Activities": "2.9.5"
- "UiPath.System.Activities": "20.10.4"
- "UiPath.UIAutomation.Activities": "20.10.10"
- "UiPath.WebAPI.Activities": "1.6.0"
```

## Argument
```
- in_ConfigFilePath: Config.xlsx path directory
```

## Description
Robot will repeats this process:
```
- Robot find new message on selected Telegram's group or contact.
- Then forward the new message using WhatsApp Desktop to WhatsApp's group or contact.
```
Robot will end the process at 23:59



```Created By Yogi Aldino```