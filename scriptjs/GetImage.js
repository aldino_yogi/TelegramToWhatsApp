function(e){
	let image64 = "";
	const imageDom = document.querySelector('div[class="MediaViewerSlide MediaViewerSlide--active"] img');
	try{
		const canvas = document.createElement('canvas');
		const context = canvas.getContext('2d');
		canvas.width = imageDom.naturalWidth;
		canvas.height = imageDom.naturalHeight;
		context.drawImage(imageDom, 0, 0);
		image64 = canvas.toDataURL().split(",")[1];
	}catch{ }
	return image64;
}