function(e, src){
	const imageDom = document.querySelector(`img[src="${src}"]`);
	imageDom.click();
	imageDom.dispatchEvent(new Event('click'));
}