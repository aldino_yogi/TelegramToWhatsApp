function(e){
	const messageDom = [...document.querySelectorAll('div[data-message-id][id]')];
	const selectedDom = messageDom[messageDom.length - 1];
	const timeDom = selectedDom.querySelector('span[class="message-time"]');
	timeDom.dispatchEvent(new MouseEvent('mouseenter', { bubbles: true }));
	return JSON.stringify({ id: selectedDom.id, data_message_id: selectedDom.getAttribute("data-message-id")})
}