function(e, id){
	const selectedDom = document.querySelector(`div[id="message${id}"][data-message-id="${id}"]`);
	const timeDom = selectedDom.querySelector('span[class="message-time"]');
	timeDom.dispatchEvent(new MouseEvent('mouseenter', { bubbles: true }));
	return JSON.stringify({ id: selectedDom.id, data_message_id: selectedDom.getAttribute("data-message-id")})
}