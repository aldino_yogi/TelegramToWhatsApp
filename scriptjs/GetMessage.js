function(e, responseString){
	let response = {type: "none", time: "", message: "", image: ""}
	const selectedDom = document.querySelector(`div[id="${JSON.parse(responseString).id}"][data-message-id="${JSON.parse(responseString).data_message_id}"]`);
	
	const timeString = selectedDom.querySelector('span[class="message-time"]').title;
	response["time"] = timeString;
	
	const canvasDom = selectedDom.querySelector('canvas[class="thumbnail"]');
	const textDom = selectedDom.querySelector('p[dir="auto"]');
	
	if(canvasDom){
		const imageDom = selectedDom.querySelector('div[class="media-inner interactive"] img');
		response["type"] = "image";
		response["image"] = imageDom.src;
		if(textDom){
			const textFull = selectedDom.querySelector('p').innerText;
			const notText = [...selectedDom.querySelector('p').childNodes].filter(item => item.innerText != undefined).map(item => item.innerText)
			const message = textFull.replace(notText.slice(-1)[0], "");
			response["message"] = message;
		}
	} else {
		if(textDom){
			const textFull = selectedDom.querySelector('p').innerText;
			const notText = [...selectedDom.querySelector('p').childNodes].filter(item => item.innerText != undefined).map(item => item.innerText)
			const message = textFull.replace(notText.slice(-1)[0], "");
			response["type"] = "text";
			response["message"] = message;
		}
	}
	return JSON.stringify(response);
}